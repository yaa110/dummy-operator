# Dummy Operator

a8s Applicant Homework: Write and Deploy a Custom Kubernetes Controller

## How to Run

- Install Dependencies:
  - [Docker](https://docs.docker.com/get-docker/)
  - [kind](https://kind.sigs.k8s.io/docs/user/quick-start/#installation)
  - [kubectl](https://kubernetes.io/docs/tasks/tools/#kubectl)
  - [make](https://www.gnu.org/software/make/#download)

- Export Variables:

```bash
export IMAGE_NAME=dummy-operator
export IMAGE_TAG=local
```

- Create a cluster:

```bash
kind create cluster
```

- Set the cluster as the default context:

```bash
kubectl config set-context kind-kind
```

- Build docker image:

```bash
make docker-build IMG=${IMAGE_NAME}:${IMAGE_TAG}
```

- Load image into the cluster:

```bash
kind load docker-image ${IMAGE_NAME}:${IMAGE_TAG}
```

- Deploy dummy controller:

```bash
make deploy IMG=${IMAGE_NAME}:${IMAGE_TAG}
```

- Watch pods to be ready:

```bash
# press ctrl-c when pod is ready
kubectl -n dummy-operator-system get pods -w
```

- Watch dummy objects and create a dummy object:

```bash
# Terminal 1:
kubectl get dummy -w

# Terminal 2:
kubectl get pods -w

# Terminal 3:
kubectl apply -k config/samples
```

- Check the logs:

```bash
kubectl -n dummy-operator-system logs `kubectl -n dummy-operator-system get pods -o NAME` | grep dummy1
```

- Delete dummy object:

```bash
kubectl delete -k config/samples
```

- Delete cluster:

```bash
kind delete cluster
```
