/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"errors"
	"fmt"
	"reflect"

	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/log"

	homeworkv1alpha1 "gitlab.com/yaa110/dummy-operator/api/v1alpha1"
)

// DummyReconciler reconciles a Dummy object
type DummyReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=homework.interview.com,resources=dummies,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=homework.interview.com,resources=dummies/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=homework.interview.com,resources=dummies/finalizers,verbs=update
//+kubebuilder:rbac:groups=core,resources=pods,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=core,resources=pods/status,verbs=get

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Dummy object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.1/pkg/reconcile
func (r *DummyReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := log.FromContext(ctx)

	var obj homeworkv1alpha1.Dummy
	if err := r.Get(ctx, req.NamespacedName, &obj); err != nil {
		if !apierrors.IsNotFound(err) {
			log.Error(err, "unable to get dummy object", "name", req.Name, "namespace", req.Namespace)
		}
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	log = log.WithValues("name", obj.Name, "namespace", obj.Namespace)

	if !obj.DeletionTimestamp.IsZero() {
		return ctrl.Result{}, nil
	}

	log.Info("reconciling", "message", obj.Spec.Message)

	if err := r.updateEchoStatus(ctx, &obj); err != nil {
		return ctrl.Result{}, err
	}

	if err := r.managePod(ctx, &obj, obj.Name); err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func (r *DummyReconciler) updateEchoStatus(ctx context.Context, obj *homeworkv1alpha1.Dummy) error {
	if obj.Status.SpecEcho == obj.Spec.Message {
		return nil
	}

	obj.Status.SpecEcho = obj.Spec.Message
	return r.Status().Update(ctx, obj)
}

func (r *DummyReconciler) managePod(ctx context.Context, obj *homeworkv1alpha1.Dummy, podName string) error {
	pod := &corev1.Pod{}
	if err := r.Get(ctx, types.NamespacedName{Name: podName, Namespace: obj.Namespace}, pod); err != nil {
		if apierrors.IsNotFound(err) {
			return r.createPod(ctx, obj)
		}

		return err
	}

	if !reflect.DeepEqual(pod.Spec, podSpec()) {
		owned, err := r.checkOwnership(ctx, obj, pod)
		if err != nil {
			return err
		}

		if !owned {
			return r.managePod(ctx, obj, podName+"-copy")
		}
	}

	if phase := string(pod.Status.Phase); obj.Status.PodStatus != phase {
		obj.Status.PodStatus = phase
		if err := r.Status().Update(ctx, obj); err != nil {
			return err
		}
	}

	switch pod.Status.Phase {
	case corev1.PodPending:
		return nil
	case corev1.PodRunning:
		return nil
	}

	return fmt.Errorf("pod failed to strat: %s", pod.Status.Reason)
}

func (r *DummyReconciler) checkOwnership(ctx context.Context, obj *homeworkv1alpha1.Dummy, pod *corev1.Pod) (bool, error) {
	err := controllerutil.SetControllerReference(obj, pod, r.Scheme)

	if errors.Is(err, &controllerutil.AlreadyOwnedError{}) { // We are the owner
		pod.Spec = podSpec()
		return true, r.Update(ctx, pod)
	}

	return false, err
}

func (r *DummyReconciler) createPod(ctx context.Context, obj *homeworkv1alpha1.Dummy) error {
	pod := &corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:      obj.Name,
			Namespace: obj.Namespace,
			Labels: map[string]string{
				"app": obj.Name,
			},
		},
		Spec: podSpec(),
	}

	if err := controllerutil.SetControllerReference(obj, pod, r.Scheme); err != nil {
		return err
	}

	return r.Create(ctx, pod)
}

func podSpec() corev1.PodSpec {
	return corev1.PodSpec{
		Containers: []corev1.Container{
			{
				Name:  "dummy-container",
				Image: "nginx",
			},
		},
	}
}

// SetupWithManager sets up the controller with the Manager.
func (r *DummyReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&homeworkv1alpha1.Dummy{}).
		Owns(&corev1.Pod{}).
		Complete(r)
}
