/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"path/filepath"
	"testing"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/envtest"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	homeworkv1alpha1 "gitlab.com/yaa110/dummy-operator/api/v1alpha1"
	//+kubebuilder:scaffold:imports
)

// These tests use Ginkgo (BDD-style Go testing framework). Refer to
// http://onsi.github.io/ginkgo/ to learn more about Ginkgo.

var cfg *rest.Config
var k8sClient client.Client
var testEnv *envtest.Environment

func TestAPIs(t *testing.T) {
	RegisterFailHandler(Fail)

	RunSpecs(t, "Controller Suite")
}

var _ = BeforeSuite(func() {
	logf.SetLogger(zap.New(zap.WriteTo(GinkgoWriter), zap.UseDevMode(true)))

	By("bootstrapping test environment")
	testEnv = &envtest.Environment{
		CRDDirectoryPaths:     []string{filepath.Join("..", "config", "crd", "bases")},
		ErrorIfCRDPathMissing: true,
	}

	var err error
	// cfg is defined in this file globally.
	cfg, err = testEnv.Start()
	Expect(err).NotTo(HaveOccurred())
	Expect(cfg).NotTo(BeNil())

	err = homeworkv1alpha1.AddToScheme(scheme.Scheme)
	Expect(err).NotTo(HaveOccurred())

	//+kubebuilder:scaffold:scheme

	k8sClient, err = client.New(cfg, client.Options{Scheme: scheme.Scheme})
	Expect(err).NotTo(HaveOccurred())
	Expect(k8sClient).NotTo(BeNil())

	k8sManager, err := ctrl.NewManager(cfg, ctrl.Options{
		Scheme: scheme.Scheme,
	})
	Expect(err).ToNot(HaveOccurred())

	err = (&DummyReconciler{
		Client: k8sManager.GetClient(),
		Scheme: k8sManager.GetScheme(),
	}).SetupWithManager(k8sManager)
	Expect(err).ToNot(HaveOccurred())

	go func() {
		defer GinkgoRecover()
		err = k8sManager.Start(context.Background())
		Expect(err).ToNot(HaveOccurred(), "failed to run manager")
	}()
})

var _ = Describe("Dummy controller", func() {
	const (
		Name      = "dummy1"
		Namespace = "default"
		Message   = "dummy message"

		timeout  = time.Second * 15
		interval = time.Millisecond * 250
	)

	Context("when creating a new dummy", func() {
		It("should create a pod", func() {
			ctx := context.Background()
			key := types.NamespacedName{Name: Name, Namespace: Namespace}
			obj := &homeworkv1alpha1.Dummy{
				ObjectMeta: metav1.ObjectMeta{
					Name:      Name,
					Namespace: Namespace,
				},
				Spec: homeworkv1alpha1.DummySpec{
					Message: Message,
				},
			}

			By("creating new dummy object")
			Expect(k8sClient.Create(ctx, obj)).Should(Succeed())

			By("creating a new pod")
			Eventually(func() error {
				pod := &corev1.Pod{}
				return k8sClient.Get(ctx, key, pod)
			}, timeout, interval).Should(Succeed())

			By("setting echo status")
			Eventually(func() string {
				obj := &homeworkv1alpha1.Dummy{}
				if err := k8sClient.Get(ctx, key, obj); err != nil {
					return ""
				}
				return obj.Status.SpecEcho
			}, timeout, interval).Should(Equal(Message))

			By("setting pod status")
			Eventually(func() string {
				obj := &homeworkv1alpha1.Dummy{}
				if err := k8sClient.Get(ctx, key, obj); err != nil {
					return ""
				}
				return obj.Status.PodStatus
			}, timeout, interval).ShouldNot(BeEmpty())

			By("deleting dummy object")
			Expect(k8sClient.Delete(ctx, obj)).Should(Succeed())

			By("terminating the pod")
			Eventually(func() bool {
				pod := &corev1.Pod{}
				return apierrors.IsNotFound(k8sClient.Get(ctx, key, pod))
			}, timeout, interval).Should(BeTrue())
		})
	})
})

var _ = AfterSuite(func() {
	By("tearing down the test environment")
	testEnv.Stop()
})
